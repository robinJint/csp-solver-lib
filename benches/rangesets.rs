use std::{iter::FromIterator, ops::Range};

use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion};
use csp_lib_rs::{rangetree::RangeTree, rangevec::RangeVec};

fn setup_rangevec<S: FromIterator<Range<isize>>>(n: isize) -> (S, S) {
    let mut set1 = Vec::new();
    let mut set2 = Vec::new();
    for i in 0..n {
        set1.push(i * 3isize..i * 3 + 2);
        set2.push(i * 3isize + n * 2..i * 3 + 2 + n * 2);
    }

    let set1 = set1.iter().cloned().collect();
    let set2 = set2.iter().cloned().collect();

    (set1, set2)
}

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("rangesets");
    for n in [10, 20, 30, 40, 50, 60, 70].iter() {
        group.bench_with_input(BenchmarkId::new("rangevec", n), n, |b, n| {
            let (set1, set2) = setup_rangevec(*n);
            b.iter(|| RangeVec::intersection(black_box(&set1), black_box(&set2)))
        });

        group.bench_with_input(BenchmarkId::new("rangetree", n), n, |b, n| {
            let (set1, set2) = setup_rangevec(*n);
            b.iter(|| RangeTree::intersection(black_box(&set1), black_box(&set2)))
        });
    }
    group.finish();

    let mut group = c.benchmark_group("optimize");
    for n in [2, 8, 32, 128, 512, 2048, 4096, 8192, 32768].iter() {
        group.bench_with_input(BenchmarkId::new("seperate", n), n, |b, n| {
            let (set1, set2) = setup_rangevec(*n);
            b.iter(|| RangeVec::merge(black_box(&set1), black_box(&set2)))
        });

        group.bench_with_input(BenchmarkId::new("inline", n), n, |b, n| {
            let (set1, set2) = setup_rangevec::<Vec<Range<isize>>>(*n);
            b.iter(|| unsafe {
                RangeVec::from_rangevec_iter(RangeVec::merge_inline(
                    black_box(&set1).into_iter().cloned(),
                    black_box(&set2).into_iter().cloned(),
                ))
            })
        });
    }
    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);

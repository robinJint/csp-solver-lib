use std::ops::Range;

use csp_lib_rs::{solve, Constraint, Csp, Domain, Opcode, SimpleSolver, VarId};

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub enum COpcode {
    Add,
    Sub,
    EQ,
    NEQ,
    GE,
    GT,
    NOT,
    AND,
    OR,
    Const,
    Var,
}

impl COpcode {
    fn to_rs(self, vars: &[VarId], x: isize) -> Opcode {
        match self {
            COpcode::Add => Opcode::Add,
            COpcode::Sub => Opcode::Sub,
            COpcode::EQ => Opcode::EQ,
            COpcode::NEQ => Opcode::NEQ,
            COpcode::GE => Opcode::GE,
            COpcode::GT => Opcode::GT,
            COpcode::NOT => Opcode::NOT,
            COpcode::AND => Opcode::AND,
            COpcode::OR => Opcode::OR,
            COpcode::Const => Opcode::Const(x),
            COpcode::Var => Opcode::Var(vars[x as usize]),
        }
    }
}

#[derive(Default)]
pub struct CCsp {
    variables: Vec<CDomain>,
    constraints: Vec<CCode>,
}

#[derive(Default)]
pub struct CCode(Vec<(COpcode, isize)>);

impl CCode {
    fn to_rs(self, vars: &[VarId]) -> Constraint {
        Constraint::VMConstraint(
            self.0
                .into_iter()
                .map(|(op, x)| op.to_rs(vars, x))
                .collect(),
        )
    }
}

#[derive(Default)]
pub struct CDomain(Vec<Range<isize>>);

impl CDomain {
    fn to_rs(self) -> Domain {
        self.0.into()
    }
}

#[no_mangle]
pub extern "C" fn csp_new() -> *mut CCsp {
    Box::leak(Box::new(CCsp::default()))
}

#[no_mangle]
pub extern "C" fn csp_new_var(csp: &mut CCsp) -> usize {
    let loc = csp.variables.len();
    csp.variables.push(CDomain::default());
    loc
}

#[no_mangle]
pub extern "C" fn csp_add_domain(csp: &mut CCsp, var: usize, start: isize, end: isize) {
    csp.variables[var].0.push(start..end)
}

#[no_mangle]
pub extern "C" fn csp_new_constraint(csp: &mut CCsp) -> usize {
    let loc = csp.variables.len();
    csp.constraints.push(CCode::default());
    loc
}

#[no_mangle]
pub extern "C" fn csp_add_opcode(csp: &mut CCsp, constr: usize, opcode: COpcode, value: isize) {
    csp.constraints[constr].0.push((opcode, value))
}

#[no_mangle]
pub unsafe extern "C" fn csp_solve(ccsp: *mut CCsp) {
    let ccsp = Box::from_raw(ccsp);
    let mut csp = Csp::default();

    let var_ids: Vec<_> = ccsp
        .variables
        .into_iter()
        .map(|domain| csp.add_var(domain.to_rs()))
        .collect();

    for constraint in ccsp.constraints {
        csp.add_constraint(constraint.to_rs(&var_ids))
    }

    let answer = solve::<SimpleSolver>(&csp);
    println!("{:?}", answer);
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

#![feature(step_trait, step_trait_ext)]

use num_bigint::BigUint;

use std::{
    collections::{HashSet, VecDeque},
    iter::Extend,
    ops::Range,
};

mod iter;
mod range_util;
pub mod rangeref;
pub mod rangevec;
mod storage;
mod vm;

use bit_set::BitSet;
use bit_vec::BitVec;
use bumpalo::Bump;
use storage::Storage;
use vm::eval;
pub use vm::{Opcode, OpcodeT};

pub type DomainSet = rangevec::RangeVec<isize>;
pub type DomainSetIter<'a> = rangevec::RangeVecIterValues<'a, isize>;

pub enum NextVariable {
    /// choose the first unassigned variable
    Simple,
    /// select the variable with the smallest domain.
    /// if it is a tie it will use the variable with the largest number of constraints on other variables
    Mrv,
}

impl Default for NextVariable {
    fn default() -> Self {
        Self::Simple
    }
}

#[derive(Default)]
pub struct Csp {
    /// the index is the VarId
    /// the vec are the constraints that affect this variable
    variables: Vec<DomainSet>,
    constraints_per_variable: Vec<Vec<usize>>,
    constraints: Vec<Constraint>,

    /// the method of selecting the next variable
    pub next_variable: NextVariable,

    /// How many solutions we will try to find
    /// None indicates all.
    ///
    /// Note that finding all solutions can take a long time
    pub number_of_solutions: Option<usize>,

    /// print debug info during the run
    pub verbose: bool,
    /// TODO
    /// use backjumping in the search
    ///
    /// when a conflict is detected the solver will go the first variable
    /// that had a constaint with the conflict.
    ///
    /// This can skip many useless states
    pub backjump: bool,
    /// TODO
    /// backjump more intelligently, states absorb the conflict set
    pub conflict_directed_backjump: bool,
    /// TODO
    /// Remember the failed states, so we can skip checking them if they come up
    /// again
    ///
    /// also called the no-good set
    pub constaint_learning: bool,

    /// use forward checking
    ///
    /// whenever we assign a new variable, we establish arc consistency on this
    /// variable
    pub forward_checking: bool,
    /// use maintaining arc consistency (MAC)
    ///
    /// same as forward_checking, but now we use ac2 instead of just one variable
    pub mac: bool,
}

impl Csp {
    pub fn variables(&self) -> impl Iterator<Item = (VarId, &DomainSet)> {
        self.variables
            .iter()
            .enumerate()
            .map(|(i, v)| (VarId(i), v))
    }

    pub fn variable_count(&self) -> usize {
        self.variables.len()
    }

    pub fn check_constraint(&self, storage: &Storage, n: usize) -> Option<bool> {
        let constraint = &self.constraints[n];

        let result = eval(&constraint.code, storage).unwrap()?;

        Some(result)
    }

    pub fn is_consistent_with<'a>(
        &'a self,
        storage: &'a Storage,
        changed_var: VarId,
    ) -> impl Iterator<Item = VarId> + 'a {
        self.constraints_per_variable[changed_var.0]
            .iter()
            .filter_map(move |n| {
                let result = self.check_constraint(storage, *n)?;

                if result {
                    None
                } else {
                    Some(vm::used_vars(&self.constraints[*n].code))
                }
            })
            .flatten()
    }

    pub fn is_consistent<'a>(&'a self, storage: &'a Storage) -> impl Iterator<Item = VarId> + 'a {
        self.constraints
            .iter()
            .enumerate()
            .filter_map(move |(i, constraint)| {
                let result = self.check_constraint(storage, i)?;

                if result {
                    None
                } else {
                    Some(vm::used_vars(&constraint.code))
                }
            })
            .flatten()
    }

    pub fn add_var(&mut self, domain: impl Into<DomainSet>) -> VarId {
        let index = self.variables.len();
        self.variables.push(domain.into());
        self.constraints_per_variable.push(Vec::new());
        VarId(index)
    }

    pub fn add_constraint(&mut self, c: impl Into<Constraint>) {
        let c = c.into();
        let new_pos = self.constraints.len();

        let used_vars = vm::used_vars(&c.code);
        for used_var in used_vars {
            self.constraints_per_variable[used_var.0].push(new_pos);
        }

        self.constraints.push(c);
    }

    pub fn node_consistent(&mut self) {
        let mut arena = Bump::new();
        for constr in &self.constraints {
            if constr.used_vars().count() == 1 {
                arena.reset();
                let new_domains = vm::infer_domains(&arena, &constr.code, &self.variables).unwrap();
                for (var, new_domain) in new_domains {
                    self.variables[var] = new_domain;
                }
            }
        }
    }

    pub fn arc_consistent(&mut self, var: VarId) {
        let mut arena = Bump::new();
        for constr_id in &self.constraints_per_variable[var.0] {
            arena.reset();
            let constr = &self.constraints[*constr_id];
            let new_domains = vm::infer_domains(&arena, &constr.code, &mut self.variables).unwrap();
            for (var, new_domain) in new_domains {
                self.variables[var] = new_domain;
            }
        }
    }

    pub fn ac2(&mut self) {
        let mut vars = std::mem::take(&mut self.variables);
        self.ac2_raw(&mut Bump::new(), &mut vars, None);
        self.variables = vars;
    }

    /// preform the ac2 algorithm
    ///
    /// changes variables and returns the original domains
    fn ac2_raw2(&self, variables: &mut [DomainSet]) -> Vec<(usize, DomainSet)> {
        let mut arena = Bump::new();
        let mut original: Vec<(usize, DomainSet)> = Vec::new();
        let mut queue: VecDeque<&Constraint> = self.constraints.iter().collect();
        while let Some(constr) = queue.pop_front() {
            if queue.len() > self.constraints.len() * 10000 {
                // println!("queue: {:?}", queue);
                panic!();
            }
            arena.reset();
            let changed_vars = vm::infer_domains(&arena, &constr.code, variables).unwrap();
            queue.extend(
                changed_vars
                    .iter()
                    .map(|(id, _)| id)
                    .map(|var| &self.constraints_per_variable[*var])
                    .flatten()
                    .map(|constr_id| &self.constraints[*constr_id]),
            );
            for (var, new_domain) in changed_vars {
                let old = std::mem::replace(&mut variables[var], new_domain);
                if !original.iter().any(|(i, _)| *i == var) {
                    original.push((var, old));
                }
            }
        }
        original
    }

    fn ac2_raw(
        &self,
        arena: &mut Bump,
        variables: &mut [DomainSet],
        var: Option<VarId>,
    ) -> Vec<(usize, DomainSet)> {
        let mut original: Vec<(usize, DomainSet)> = Vec::new();
        let mut iter_set = if let Some(var) = var {
            let mut iter_set = BitSet::with_capacity(self.constraints.len());
            iter_set.extend(self.constraints_per_variable[var.0].iter().copied());
            iter_set
        } else {
            BitSet::from_bit_vec(BitVec::from_elem(self.constraints.len(), true))
        };
        let mut new_set = BitSet::with_capacity(self.constraints.len());

        while !iter_set.is_empty() {
            for i in iter_set.iter() {
                arena.reset();
                let constr = &self.constraints[i];
                let changed_vars = vm::infer_domains(&arena, &constr.code, variables).unwrap();
                new_set.extend(
                    changed_vars
                        .iter()
                        .map(|(id, _)| id)
                        .map(|var| &self.constraints_per_variable[*var])
                        .flatten()
                        .copied(),
                );
                for (var, new_domain) in changed_vars {
                    // println!("changing {} to {:?}->{:?}", var, variables[var], new_domain);
                    let old = std::mem::replace(&mut variables[var], new_domain);
                    if !original.iter().any(|(i, _)| *i == var) {
                        original.push((var, old));
                    }
                }
                new_set.remove(i);
            }

            std::mem::swap(&mut iter_set, &mut new_set);
            new_set.clear()
        }

        original
    }
}

struct CspState {
    visited_states: usize,
    found_solutions: Vec<Box<[isize]>>,
    domains: Box<[DomainSet]>,

    storage: Storage,
    arena: Bump,
}

impl CspState {
    fn new(storage: Storage, domains: &[DomainSet]) -> Self {
        assert_eq!(domains.len(), storage.variable_count());

        Self {
            visited_states: 0,
            found_solutions: Vec::new(),
            domains: domains
                .iter()
                .cloned()
                .collect::<Vec<_>>()
                .into_boxed_slice(),
            storage,
            arena: Bump::new(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Constraint {
    code: Vec<Opcode>,
}

impl Constraint {
    pub fn used_vars<'a>(&'a self) -> impl Iterator<Item = VarId> + 'a {
        vm::used_vars(&self.code)
    }
}

impl From<Vec<Opcode>> for Constraint {
    fn from(code: Vec<Opcode>) -> Self {
        Self { code }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct VarId(usize);

impl VarId {
    pub fn as_usize(self) -> usize {
        self.0
    }
}

#[derive(Debug, Clone, Default)]
pub struct Domain(DomainSet);

impl Domain {
    pub fn len(&self) -> usize {
        self.0.len() as usize
    }

    pub fn empty() -> Self {
        Self(DomainSet::default())
    }
}

impl From<Range<isize>> for Domain {
    fn from(range: Range<isize>) -> Self {
        Domain(range.into())
    }
}

impl From<Vec<Range<isize>>> for Domain {
    fn from(vec: Vec<Range<isize>>) -> Self {
        Self(DomainSet::from_vec(vec))
    }
}

impl From<DomainSet> for Domain {
    fn from(ds: DomainSet) -> Self {
        Self(ds)
    }
}

#[derive(Debug)]
pub struct CspResult {
    pub all_possible_states: num_bigint::BigUint,
    pub visited_states: usize,
    pub solutions: Vec<Box<[isize]>>,
}

pub fn solve(csp: &Csp) -> CspResult {
    let all_possible_states: num_bigint::BigUint = csp
        .variables
        .iter()
        .map::<BigUint, _>(|domain| domain.len().into())
        .product();

    let storage = Storage::new(csp.variable_count());
    let mut state = CspState::new(storage, &csp.variables);

    backtrack(csp, &mut state).ok();

    CspResult {
        all_possible_states,
        visited_states: state.visited_states,
        solutions: state.found_solutions,
    }
}

fn interleave_opt(
    csp: &Csp,
    arena: &mut Bump,
    domains: &mut [DomainSet],
    var: VarId,
) -> Vec<(usize, DomainSet)> {
    if csp.mac {
        csp.ac2_raw(arena, domains, Some(var))
    } else if csp.forward_checking {
        let mut original = Vec::new();
        for constr_id in &csp.constraints_per_variable[var.0] {
            let constr = &csp.constraints[*constr_id];
            arena.reset();
            for (id, domain) in vm::infer_domains(&arena, &constr.code, domains).unwrap() {
                let old = std::mem::replace(&mut domains[id], domain);
                original.push((id, old));
            }
        }
        original
    } else {
        Vec::new()
    }
}

fn select_unassigned_variable(csp: &Csp, storage: &Storage) -> VarId {
    match csp.next_variable {
        NextVariable::Simple => storage.first_unset().unwrap(),
        NextVariable::Mrv => {
            csp.variables()
                .filter(|(v, _)| !storage.is_set(*v))
                .min_by_key(|(VarId(v), domain)| {
                    (domain.len(), csp.constraints_per_variable[*v].len())
                })
                .unwrap()
                .0
        }
    }
}

/// improvements
/// - use a fully peallocated bitvec for the conflict set
/// - use conflict-set again
///
/// NOTEs
/// - use same tactic for domains as for vars
///   reset them after use, thus avoiding allocations
fn backtrack(csp: &Csp, state: &mut CspState) -> Result<(), HashSet<VarId>> {
    if state.storage.is_complete() {
        state
            .found_solutions
            .push(state.storage.clone().into_boxed_slice());
        return Ok(());
    }

    let var = select_unassigned_variable(csp, &state.storage);
    let domain = state.domains[var.0].clone();
    let mut conflict_set = HashSet::new();

    for value in domain.iter() {
        state.visited_states += 1;
        state.storage.set(var, value);
        let old_domain = std::mem::replace(
            &mut state.domains[var.0],
            DomainSet::single(value..value + 1),
        );

        {
            let mut conflicts = csp.is_consistent_with(&state.storage, var);
            if let Some(c) = conflicts.next() {
                conflict_set.insert(c);
                conflict_set.extend(conflicts);
                conflict_set.remove(&var);
                state.storage.unset(var);
                state.domains[var.0] = old_domain;
                continue;
            }
        }

        let originals = interleave_opt(csp, &mut state.arena, &mut state.domains, var);

        match backtrack(csp, state) {
            Ok(()) if Some(state.found_solutions.len()) == csp.number_of_solutions => return Ok(()),
            Ok(()) => {
                state.storage.unset(var);
                for (id, domain) in originals {
                    state.domains[id] = domain;
                }
                state.domains[var.0] = old_domain;
                // continue as we want to find more solutions
            }
            Err(rec_conflict_set) => {
                state.storage.unset(var);
                for (id, domain) in originals {
                    state.domains[id] = domain;
                }
                state.domains[var.0] = old_domain;
                conflict_set.extend(&rec_conflict_set);
                if !rec_conflict_set.contains(&var) {
                    // enable conflict_set again
                    // return Err(conflict_set);
                }
            }
        }
    }

    if conflict_set.is_empty() {
        Ok(())
    } else {
        Err(conflict_set)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn nqueens() -> Csp {
        let mut csp = Csp::default();
        let vars: Vec<_> = (0..8).map(|_| csp.add_var(0..8)).collect();

        for i in 0..8 {
            for j in i + 1..8 {
                csp.add_constraint(vec![
                    Opcode::Var(vars[i]),
                    Opcode::Var(vars[j]),
                    Opcode::NEQ,
                ])
            }
        }

        for q in 0..8 {
            for row in 1..(8 - q) {
                csp.add_constraint(vec![
                    Opcode::Const(row as isize),
                    Opcode::Var(vars[q]),
                    Opcode::Var(vars[q + row]),
                    Opcode::Sub,
                    Opcode::NEQ,
                ])
            }
        }
        csp
    }

    #[test]
    fn basic_problems() {
        assert!(solve(&nqueens()).solutions.len() > 0)
    }

    fn run(csp: &Csp) -> Vec<isize> {
        solve(csp).solutions[0].to_vec()
    }

    #[test]
    fn empty() {
        let csp = Csp::default();
        let correct: Vec<isize> = Vec::new();
        assert_eq!(run(&csp), correct)
    }

    #[test]
    fn no_constraints() {
        let mut csp = Csp::default();
        csp.add_var(3..10);
        assert_eq!(run(&csp), vec![3])
    }

    #[test]
    fn single_var() {
        let mut csp = Csp::default();
        let var = csp.add_var(3..10);
        csp.add_constraint(vec![Opcode::Const(9), Opcode::Var(var), Opcode::EQ]);
        assert_eq!(run(&csp), vec![9])
    }

    #[test]
    fn impossible() {
        let mut csp = Csp::default();
        let var = csp.add_var(1..10);
        csp.add_constraint(vec![Opcode::Const(11), Opcode::Var(var), Opcode::EQ]);
        assert_eq!(solve(&csp).solutions.len(), 0)
    }
}

use std::{
    cmp,
    iter::{ExactSizeIterator, Peekable},
    ops::Range,
};
pub struct MergeIter<It1: Iterator, It2: Iterator, F> {
    it1: Peekable<It1>,
    it2: Peekable<It2>,
    f: F,
}

impl<It1, It2, T, FOrd> Iterator for MergeIter<It1, It2, FOrd>
where
    It1: Iterator<Item = T>,
    It2: Iterator<Item = T>,
    FOrd: Fn(&T, &T) -> cmp::Ordering,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match (self.it1.peek(), self.it2.peek()) {
            (Some(l), Some(r)) => match (self.f)(l, r) {
                cmp::Ordering::Less => self.it1.next(),
                _ => self.it2.next(),
            },
            (Some(_), None) => self.it1.next(),
            (None, Some(_)) => self.it2.next(),
            (None, None) => None,
        }
    }

    fn fold<B, F>(mut self, init: B, mut f: F) -> B
    where
        F: FnMut(B, Self::Item) -> B,
    {
        let mut accum = init;

        while let (Some(l), Some(r)) = (self.it1.peek(), self.it2.peek()) {
            match (self.f)(l, r) {
                cmp::Ordering::Less => {
                    accum = f(accum, self.it1.next().unwrap());
                }
                _ => {
                    accum = f(accum, self.it2.next().unwrap());
                }
            }
        }

        let accum = self.it1.fold(accum, |accum, x| f(accum, x));
        let accum = self.it2.fold(accum, f);

        accum
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let (min1, max1) = self.it1.size_hint();
        let (min2, max2) = self.it2.size_hint();

        let max = match (max1, max2) {
            (Some(max1), Some(max2)) => Some(max1.saturating_add(max2)),
            _ => None,
        };

        (min1.saturating_add(min2), max)
    }
}

impl<It1, It2, T, F> ExactSizeIterator for MergeIter<It1, It2, F>
where
    It1: ExactSizeIterator + Iterator<Item = T>,
    It2: ExactSizeIterator + Iterator<Item = T>,
    F: Fn(&T, &T) -> cmp::Ordering,
{
}

pub fn merge_iter<T, It1, It2>(
    it1: It1,
    it2: It2,
) -> MergeIter<It1, It2, fn(&T, &T) -> cmp::Ordering>
where
    It1: Iterator<Item = T>,
    It2: Iterator<Item = T>,
    T: Ord,
{
    MergeIter {
        it1: it1.peekable(),
        it2: it2.peekable(),
        f: Ord::cmp,
    }
}

pub fn merge_iter_by<T, It1, It2, F>(it1: It1, it2: It2, f: F) -> MergeIter<It1, It2, F>
where
    It1: Iterator<Item = T>,
    It2: Iterator<Item = T>,
    F: Fn(&T, &T) -> cmp::Ordering,
{
    MergeIter {
        it1: it1.peekable(),
        it2: it2.peekable(),
        f,
    }
}

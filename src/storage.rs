use bitvec::{boxed::BitBox, prelude::bitvec};

use crate::VarId;

#[derive(Clone)]
pub struct Storage {
    values: Box<[isize]>,
    flags: bitvec::boxed::BitBox,
    set_value_count: usize,
}

impl Storage {
    pub fn new(variable_count: usize) -> Self {
        Self {
            values: vec![0; variable_count].into_boxed_slice(),
            flags: BitBox::new(&bitvec![0; variable_count]),
            set_value_count: 0,
        }
    }

    pub fn get(&self, var: VarId) -> Option<isize> {
        if *self.flags.get(var.0).expect("internal error") {
            Some(self.values[var.0])
        } else {
            None
        }
    }

    pub fn is_set(&self, var: VarId) -> bool {
        *self.flags.get(var.0).expect("internal error")
    }

    pub fn set(&mut self, var: VarId, value: isize) {
        self.values[var.0] = value;
        let old = self.flags.get(var.0).expect("internal error");
        if !old {
            self.set_value_count += 1;
        }
        self.flags.set(var.0, true);
    }

    pub fn unset(&mut self, var: VarId) {
        let old = self.flags.get(var.0).expect("internal error");
        if *old {
            self.set_value_count -= 1;
        }
        self.flags.set(var.0, false)
    }

    pub fn first_unset(&self) -> Option<VarId> {
        self.flags
            .iter()
            .enumerate()
            .find(|(_, flag)| !**flag)
            .map(|pos| VarId(pos.0))
    }

    pub fn variable_count(&self) -> usize {
        self.values.len()
    }

    pub fn set_variable_count(&self) -> usize {
        self.set_value_count
    }

    pub fn is_complete(&self) -> bool {
        self.variable_count() == self.set_value_count
    }

    pub fn into_boxed_slice(self) -> Box<[isize]> {
        self.values
    }
}

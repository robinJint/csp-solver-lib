use std::{
    cmp, fmt,
    iter::{once, Step},
    mem::MaybeUninit,
    ops::{Deref, Range},
};

use crate::{range_util, rangevec::RangeVec};
use bumpalo::Bump;
use smallvec::SmallVec;
use typed_arena::Arena;

struct Buf<'a, T> {
    next_free: usize,
    buf: &'a mut [T],
}

impl<'a, T: Clone> Buf<'a, T> {
    fn new(buf: &'a mut [T]) -> Self {
        Self { next_free: 0, buf }
    }

    fn push(&mut self, item: T) {
        self.buf[self.next_free] = item;
        self.next_free += 1;
    }

    fn finish(self) -> &'a mut [T] {
        &mut self.buf[..self.next_free]
    }
}

fn new_buf<'a, T: Clone>(alloc: &'a Bump, n: usize, base: &Range<T>) -> Buf<'a, Range<T>> {
    Buf::new(alloc.alloc_slice_fill_clone(n, &base))
}

#[derive(Clone, Copy)]
pub struct RangeRef<'a, T> {
    alloc: &'a Bump,
    data: &'a [Range<T>],
}

impl<'a, T> Deref for RangeRef<'a, T> {
    type Target = [Range<T>];
    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl<'a, T: PartialEq> PartialEq for RangeRef<'a, T> {
    fn eq(&self, other: &Self) -> bool {
        self.deref().eq(other.deref())
    }
}

impl<'a, T: fmt::Debug> fmt::Debug for RangeRef<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.data.fmt(f)
    }
}

impl<'a, T: Ord + Copy + Step + fmt::Debug + Default> RangeRef<'a, T> {
    pub fn single(range: Range<T>, alloc: &'a Bump) -> Self {
        Self::from_iter(once(range), alloc)
    }

    pub fn empty(alloc: &'a Bump) -> Self {
        Self { data: &[], alloc }
    }

    pub fn is_unit(&self) -> bool {
        self.data.len() == 1 && Step::forward(self.data[0].start, 1) == self.data[0].end
    }

    pub fn from_vec(data: &'a mut [Range<T>], alloc: &'a Bump) -> Self {
        data.sort_unstable_by_key(|r| r.start);

        Self::from_sorted_vec(data, alloc)
    }

    pub fn from_iter(data: impl ExactSizeIterator<Item = Range<T>>, alloc: &'a Bump) -> Self {
        let mut buf = new_buf(alloc, data.len(), &(T::default()..T::default()));
        for item in data {
            buf.push(item)
        }

        Self::from_sorted_vec(buf.finish(), alloc)
    }

    pub fn from_rangeref<'b, 'c>(data: RangeRef<'b, T>, alloc: &'c Bump) -> RangeRef<'c, T> {
        RangeRef {
            data: alloc.alloc_slice_clone(&data.data),
            alloc,
        }
    }

    pub fn from_sorted_vec(data: &'a mut [Range<T>], alloc: &'a Bump) -> Self {
        Self {
            data: Self::optimize(data),
            alloc,
        }
    }

    pub fn from_rangevec(rangevec: &'a RangeVec<T>, alloc: &'a Bump) -> Self {
        Self {
            data: &*rangevec,
            alloc,
        }
    }

    pub fn to_rangevec(&self) -> RangeVec<T> {
        RangeVec::from_vec(SmallVec::from_vec(self.data.to_vec()))
    }

    // pub unsafe fn from_rangevec_iter(it: impl Iterator<Item = Range<T>>) -> Self {
    //     let mut data: SmallVec<Range<T>> = SmallVec::with_capacity(it.size_hint().0);
    //     it.for_each(|range| {
    //         let end = data.as_mut_ptr().add(data.len());
    //         std::ptr::write(end, range);
    //         data.set_len(data.len() + 1);
    //     });

    //     Self { data: data.into() }
    // }

    #[must_use = "the old data is most likely invalid"]
    fn optimize(data: &mut [Range<T>]) -> &mut [Range<T>] {
        if data.len() < 2 {
            return data;
        }

        let mut current = 0;
        let mut next = 1;
        while next < data.len() {
            if data[current].end > data[next].end {
                next += 1;
            } else if data[current].end >= data[next].start {
                data[current].end = data[next].end;
                next += 1;
            } else {
                current += 1;
                data[current] = data[next].clone();
                next += 1;
            }
        }

        &mut data[..current + 1]
    }

    pub fn iter_ranges(&self) -> std::slice::Iter<Range<T>> {
        self.data.iter()
    }

    pub fn iter(&'a self) -> impl Iterator<Item = T> + 'a {
        self.iter_ranges().cloned().flatten()
    }

    pub fn contains(&self, n: &T) -> bool {
        if self.data.is_empty() {
            return false;
        }

        let mut start = 0;
        let mut end = self.data.len() - 1;

        while start <= end {
            let middle = start + (end - start) / 2;
            let r = &self.data[middle];
            if n < &r.start {
                if middle == start {
                    return false;
                } else {
                    end = middle - 1;
                }
            } else if n >= &r.end {
                start = middle + 1;
            } else {
                return true;
            }
        }

        false
    }

    pub fn merge(&self, other: &Self) -> Self {
        let mut new_vec = self.new_buf(self.data.len() + other.data.len());

        let mut it1 = self.iter_ranges().peekable();
        let mut it2 = other.iter_ranges().peekable();

        while let (Some(l), Some(r)) = (it1.peek(), it2.peek()) {
            if l.start < r.start {
                new_vec.push((*l).clone());
                it1.next().unwrap();
            } else {
                new_vec.push((*r).clone());
                it2.next().unwrap();
            }
        }

        for item in it1 {
            new_vec.push(item.clone())
        }
        for item in it2 {
            new_vec.push(item.clone())
        }
        let res = Self::from_sorted_vec(new_vec.finish(), self.alloc);
        res
    }

    fn new_buf(self, n: usize) -> Buf<'a, Range<T>> {
        new_buf(self.alloc, n, &(T::default()..T::default()))
    }

    pub fn intersection(&self, other: &Self) -> Self {
        let mut new_vec = self.new_buf(cmp::max(self.data.len(), other.data.len()));
        let mut l_it = self.iter_ranges().peekable();
        let mut r_it = other.iter_ranges().peekable();

        while let (Some(l), Some(r)) = (l_it.peek(), r_it.peek()) {
            let (l, r, l_it, r_it) = if l.start < r.start {
                (*l, *r, &mut l_it, &mut r_it)
            } else {
                (*r, *l, &mut r_it, &mut l_it)
            };

            if l.end > r.start {
                if l.end >= r.end {
                    r_it.next().unwrap();
                    new_vec.push(r.start..r.end)
                } else {
                    l_it.next().unwrap();
                    new_vec.push(r.start..l.end)
                }
            } else {
                l_it.next().unwrap();
            }
        }

        Self {
            data: new_vec.finish(),
            alloc: self.alloc,
        }
    }

    pub fn remove(&self, val: T) -> Self {
        let mut data = self.new_buf(self.data.len() + 1);

        for current in self.data {
            if current.contains(&val) {
                let l = current.start..val;
                let r = Step::forward(val, 1)..current.end;

                if range_util::is_empty(&l) {
                    data.push(r);
                } else if range_util::is_empty(&r) {
                    data.push(l);
                } else {
                    data.push(l);
                    data.push(r);
                }
            } else {
                data.push(current.clone());
            }
        }

        Self {
            data: data.finish(),
            alloc: self.alloc,
        }
    }

    pub fn product_with(
        &self,
        other: &Self,
        mut f: impl FnMut(Range<T>, Range<T>) -> Range<T>,
    ) -> Self {
        let mut data = self.new_buf(self.data.len() * other.data.len());

        for r1 in self.iter_ranges() {
            for r2 in other.iter_ranges() {
                data.push(f(r1.clone(), r2.clone()))
            }
        }

        Self::from_vec(data.finish(), self.alloc)
    }

    pub fn len(&self) -> usize {
        self.iter_ranges()
            .map(|r| T::steps_between(&r.start, &r.end).unwrap())
            .sum()
    }

    /// take the product with each individual value instead of the range
    pub fn product_with_values(&self, other: &Self, mut f: impl FnMut(T, T) -> Range<T>) -> Self {
        let mut data = self.new_buf(self.len() * other.len());

        for r1 in self.iter() {
            for r2 in other.iter() {
                data.push(f(r1.clone(), r2.clone()))
            }
        }

        Self::from_vec(data.finish(), self.alloc)
    }

    pub fn filter(&self, mut f: impl FnMut(T) -> bool) -> Self {
        let mut data = self.new_buf(self.len());

        for value in self.iter() {
            if f(value) {
                data.push(value..Step::forward(value, 1));
            }
        }

        Self::from_vec(data.finish(), self.alloc)
    }
}

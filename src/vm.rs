use log::{debug, info, warn};

use crate::{range_util, rangeref::RangeRef, rangevec::RangeVec, storage::Storage, VarId};
use bumpalo::{collections as bump, Bump};
use std::ops::{Deref, Range};
use typed_arena::Arena;

#[derive(Debug)]
pub enum Error {
    EmptyStack,
    MissingValue,
    DivByZero,
    NotSupported,
}

#[derive(Debug, Clone, Copy)]
pub enum OpcodeT<T> {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Abs,
    EQ,
    NEQ,
    GE,
    GT,
    LE,
    LT,
    NOT,
    AND,
    BOR,
    XOR,
    OR,
    Const(isize),
    Var(T),
}

impl<T> OpcodeT<T> {
    pub fn map_t<K>(self, f: impl FnOnce(T) -> K) -> OpcodeT<K> {
        match self {
            OpcodeT::Add => OpcodeT::Add,
            OpcodeT::Sub => OpcodeT::Sub,
            OpcodeT::Mul => OpcodeT::Mul,
            OpcodeT::Div => OpcodeT::Div,
            OpcodeT::Mod => OpcodeT::Mod,
            OpcodeT::Abs => OpcodeT::Abs,
            OpcodeT::EQ => OpcodeT::EQ,
            OpcodeT::NEQ => OpcodeT::NEQ,
            OpcodeT::GE => OpcodeT::GE,
            OpcodeT::GT => OpcodeT::GT,
            OpcodeT::LE => OpcodeT::LE,
            OpcodeT::LT => OpcodeT::LT,
            OpcodeT::NOT => OpcodeT::NOT,
            OpcodeT::AND => OpcodeT::AND,
            OpcodeT::OR => OpcodeT::OR,
            OpcodeT::BOR => OpcodeT::BOR,
            OpcodeT::XOR => OpcodeT::XOR,
            OpcodeT::Const(x) => OpcodeT::Const(x),
            OpcodeT::Var(x) => OpcodeT::Var(f(x)),
        }
    }
}

pub type Opcode = OpcodeT<VarId>;

pub fn used_vars<'a>(code: &'a [Opcode]) -> impl Iterator<Item = VarId> + 'a {
    code.iter().filter_map(|opcode| {
        if let Opcode::Var(var) = opcode {
            Some(*var)
        } else {
            None
        }
    })
}

fn range_div(x: isize, y: isize) -> Range<isize> {
    if y == 0 {
        0..0
    } else {
        x / y..x / y + 1
    }
}

fn range_mod(x: isize, y: isize) -> Range<isize> {
    if y == 0 {
        0..0
    } else {
        x % y..x % y + 1
    }
}

fn step(opcode: Opcode, storage: &Storage, stack: &mut Vec<isize>) -> Result<(), Error> {
    match opcode {
        Opcode::Add => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push(x + y);
        }
        Opcode::Sub => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push(x - y);
        }
        Opcode::Mul => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push(x * y);
        }
        Opcode::Div => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;

            if y == 0 {
                return Err(Error::DivByZero);
            }

            stack.push(x / y);
        }
        Opcode::Mod => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;

            if y == 0 {
                return Err(Error::DivByZero);
            }

            stack.push(x % y);
        }
        Opcode::Abs => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push(x.abs())
        }
        Opcode::Const(val) => stack.push(val),
        Opcode::Var(var) => stack.push(storage.get(var).ok_or(Error::MissingValue)?),
        Opcode::NEQ => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push((x != y) as isize);
        }
        Opcode::EQ => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push((x == y) as isize);
        }
        Opcode::BOR => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push(x | y);
        }
        Opcode::XOR => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push(x ^ y);
        }
        Opcode::LT => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push((x < y) as isize);
        }
        Opcode::LE => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push((x <= y) as isize);
        }
        Opcode::GT => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push((x > y) as isize);
        }
        Opcode::GE => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push((x >= y) as isize);
        }
        Opcode::NOT => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let x_as_bool = x >= 1;
            stack.push((!x_as_bool) as isize);
        }
        Opcode::AND => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push((x >= 1 && y >= 1) as isize);
        }
        Opcode::OR => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push((x >= 1 || y >= 1) as isize);
        }
    }

    Ok(())
}

pub fn eval(code: &[Opcode], storage: &Storage) -> Result<Option<bool>, Error> {
    let mut stack = Vec::new();
    for opcode in code {
        match step(*opcode, storage, &mut stack) {
            Ok(()) => {}
            Err(Error::MissingValue) => return Ok(None),
            Err(Error::DivByZero) => return Ok(None),
            Err(e) => return Err(e),
        }
    }

    Ok(Some(stack.pop().ok_or(Error::EmptyStack)? != 0))
}

type RangeSet<'a> = RangeRef<'a, isize>;

fn step_domain<'a>(
    arena: &'a Bump,
    opcode: Opcode,
    storage: &'a [RangeVec<isize>],
    stack: &mut Vec<RangeSet<'a>>,
) -> Result<(Opcode, Option<RangeSet<'a>>, Option<RangeSet<'a>>), Error> {
    let res = match opcode {
        Opcode::Const(val) => {
            stack.push(RangeSet::single(val..val + 1, arena));
            (None, None)
        }
        Opcode::Var(var) => {
            stack.push(RangeRef::from_rangevec(&storage[var.0], arena));
            (Some(RangeRef::from_rangevec(&storage[var.0], arena)), None)
        }
        Opcode::EQ => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let result = RangeSet::intersection(&x, &y);
            stack.push(result.clone());
            (Some(x), Some(y))
        }
        Opcode::NEQ => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;

            let result: RangeSet = if y.is_unit() && x.is_unit() {
                let y = y.iter().next().unwrap();
                let x = x.iter().next().unwrap();

                if x == y {
                    RangeSet::empty(arena)
                } else {
                    RangeSet::from_iter([x..x + 1, y..y + 1].iter().cloned(), arena)
                }
            } else if y.is_unit() {
                let mut result = x.clone();
                result.remove(y.iter().next().unwrap())
            } else if x.is_unit() {
                let mut result = y.clone();
                result.remove(x.iter().next().unwrap())
            } else {
                RangeSet::merge(&x, &y)
            };

            stack.push(result);
            (Some(x), Some(y))
        }
        Opcode::Add => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let result = RangeSet::product_with(&x, &y, range_util::add);

            stack.push(result.clone());
            (Some(x), Some(y))
        }
        Opcode::Sub => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let result = RangeSet::product_with(&x, &y, range_util::sub);

            stack.push(result.clone());
            (Some(y), Some(x))
        }
        Opcode::Mul => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let result = RangeSet::product_with_values(&x, &y, |x, y| x * y..x * y + 1);

            stack.push(result.clone());
            (Some(x), Some(y))
        }
        Opcode::Div => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let result = RangeSet::product_with_values(&x, &y, range_div);

            stack.push(result.clone());
            (Some(y), Some(x))
        }
        Opcode::Mod => {
            let y = stack.pop().ok_or(Error::EmptyStack)?;
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let result = RangeSet::product_with_values(&x, &y, range_mod);

            stack.push(result.clone());
            (Some(y), Some(x))
        }
        Opcode::Abs => {
            let x = stack.pop().ok_or(Error::EmptyStack)?;
            let result = RangeRef::from_iter(
                x.iter_ranges().map(|r| crate::range_util::abs(r.clone())),
                arena,
            );

            stack.push(result);
            (Some(x), None)
        }
        _ => return Err(Error::NotSupported),
    };

    Ok((opcode, res.0, res.1))
}

fn rev_step_domain<'a>(
    arena: &'a Bump,
    (opcode, arg1, arg2): (Opcode, Option<RangeSet<'a>>, Option<RangeSet<'a>>),
    storage: &[RangeVec<isize>],
    stack: &mut Vec<RangeSet<'a>>,
) -> Result<Option<(usize, RangeSet<'a>)>, Error> {
    debug!("{:?} ({:?} {:?})", opcode, arg1, arg2);
    match opcode {
        Opcode::Const(_) => {
            stack.pop().ok_or(Error::EmptyStack)?;
        }
        Opcode::Var(var) => {
            let domain = stack.pop().ok_or(Error::EmptyStack)?;
            if *domain == *storage[var.0] {
                info!("no changes: {:?}", domain);
            } else {
                warn!("new domain: {:?} -> {:?}", storage[var.0], domain);
                return Ok(Some((var.0, domain)));
            }
        }
        Opcode::EQ => {
            let res = stack.pop().ok_or(Error::EmptyStack)?;
            stack.push(res.clone());
            stack.push(res);
        }
        Opcode::NEQ => {
            let res = stack.pop().ok_or(Error::EmptyStack)?;
            let arg1 = arg1.unwrap();
            let arg2 = arg2.unwrap();
            info!("step_domain: {:?} != {:?} => {:?}", arg1, arg2, res);

            if arg2.is_unit() {
                stack.push(arg2)
            } else {
                stack.push(RangeSet::intersection(&res, &arg2));
            }

            if arg1.is_unit() {
                stack.push(arg1)
            } else {
                stack.push(RangeSet::intersection(&res, &arg1));
            }
        }
        Opcode::Add => {
            let result = stack.pop().ok_or(Error::EmptyStack)?;
            let arg1 = arg1.unwrap();
            let arg2 = arg2.unwrap();

            let x = RangeSet::product_with(&result, &arg1, |res, y| {
                // res = x + y => x = res - y
                range_util::sub(res, y)
            });
            let x = RangeSet::intersection(&x, &arg2);

            let y = RangeSet::product_with(&result, &arg2, |res, x| {
                // res = x + y => y = res - x
                range_util::sub(res, x)
            });

            let y = RangeSet::intersection(&y, &arg1);

            stack.push(x);
            stack.push(y);
        }
        Opcode::Sub => {
            let result = stack.pop().ok_or(Error::EmptyStack)?;
            let y_arg = arg1.unwrap();
            let x_arg = arg2.unwrap();

            let x = RangeSet::product_with(&result, &y_arg, |res, y| {
                // res = x - y => x = res + y
                range_util::add(res, y)
            });
            let x = RangeSet::intersection(&x, &x_arg);

            let y = RangeSet::product_with(&result, &x_arg, |res, x| {
                // res = x - y => y = y = x - res
                range_util::sub(x, res)
            });

            let y = RangeSet::intersection(&y, &y_arg);

            stack.push(x);
            stack.push(y);
        }
        Opcode::Mul => {
            let result = stack.pop().ok_or(Error::EmptyStack)?;
            let y_arg = arg1.unwrap();
            let x_arg = arg2.unwrap();

            // res = x*y => x = res/y
            let x = RangeSet::product_with_values(&result, &y_arg, range_div);
            let x = RangeSet::intersection(&x, &x_arg);
            // res = x*y => y = res/x
            let y = RangeSet::product_with_values(&result, &x_arg, range_div);
            let y = RangeSet::intersection(&y, &y_arg);

            stack.push(x);
            stack.push(y);
        }
        Opcode::Div => {
            let result = stack.pop().ok_or(Error::EmptyStack)?;
            let y_arg = arg1.unwrap();
            let x_arg = arg2.unwrap();

            // res = x/y => x = res*y
            let x = RangeSet::product_with_values(&result, &y_arg, |res, y| res * y..res * y + 1);
            let x = RangeSet::intersection(&x, &x_arg);
            // res = x/y => y = x/res
            let y = RangeSet::product_with_values(&x_arg, &result, range_div);
            let y = RangeSet::intersection(&y, &y_arg);

            stack.push(x);
            stack.push(y);
        }
        Opcode::Mod => {
            let result = stack.pop().ok_or(Error::EmptyStack)?;
            let y_arg = arg1.unwrap();
            let x_arg = arg2.unwrap();

            let x = x_arg.filter(|x| y_arg.iter().any(|y| result.contains(&(x % y))));

            let y = y_arg.filter(|y| x_arg.iter().any(|x| result.contains(&(x % y))));

            stack.push(x);
            stack.push(y);
        }
        Opcode::Abs => {
            let result = stack.pop().ok_or(Error::EmptyStack)?;
            let arg = arg1.unwrap();
            let negated = result.iter_ranges().map(|r| range_util::negate(r.clone()));
            let negated = RangeRef::from_iter(negated, arena);
            let all_options = result.merge(&negated);

            let reduced = all_options.intersection(&arg);

            stack.push(reduced);
        }
        _ => return Err(Error::NotSupported),
    }

    Ok(None)
}
pub fn infer_domains<'a, 'bump: 'a>(
    arena: &'bump Bump,
    code: &[Opcode],
    storage: &'a [RangeVec<isize>],
) -> Result<bump::Vec<'bump, (usize, RangeVec<isize>)>, Error> {
    match infer_domains_inner(arena, code, storage) {
        Err(Error::NotSupported) => Ok(bump::Vec::new_in(arena)),
        x => x,
    }
}

fn infer_domains_inner<'a, 'bump: 'a>(
    arena: &'bump Bump,
    code: &[Opcode],
    storage: &'a [RangeVec<isize>],
) -> Result<bump::Vec<'bump, (usize, RangeVec<isize>)>, Error> {
    info!("starting: {:?}", code);
    let mut stack = Vec::with_capacity(code.len());
    let mut results = Vec::with_capacity(code.len());
    for opcode in code {
        results.push(step_domain(&arena, *opcode, storage, &mut stack)?);
        info!(
            "step {:?}({:?}) => {:?}",
            opcode,
            results.last().unwrap(),
            stack
        );
    }

    let mut changed = bump::Vec::with_capacity_in(8, arena);
    for result in results.into_iter().rev() {
        let changed_var = rev_step_domain(&arena, result, storage, &mut stack)?;
        if let Some((i, var)) = changed_var {
            changed.push((i, var.to_rangevec()))
        }
    }

    Ok(changed)
}

#[cfg(test)]
mod tests {
    use super::*;

    pub fn infer_domains_inplace(
        code: &Vec<Opcode>,
        storage: &mut [RangeVec<isize>],
    ) -> Result<(), Error> {
        // enable to see debug output, slows down tests
        env_logger::builder().is_test(true).try_init().ok();

        let arena = Bump::new();
        for (i, new) in infer_domains(&arena, code, storage)?
            .into_iter()
            .collect::<Vec<_>>()
        {
            storage[i] = new
        }
        Ok(())
    }

    #[test]
    fn eq_constant() {
        let code = vec![Opcode::Const(5), Opcode::Var(VarId(0)), Opcode::EQ];
        let mut storage = vec![[1..3, 5..15, 20..23].iter().cloned().collect()];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![5])
    }

    #[test]
    fn eq_constant_add() {
        let code = vec![
            Opcode::Const(5),
            Opcode::Var(VarId(0)),
            Opcode::Add,
            Opcode::Const(10),
            Opcode::EQ,
        ];
        let mut storage = vec![[1..3, 5..15, 20..23].iter().cloned().collect()];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![5])
    }

    #[test]
    fn eq_two_vars() {
        let code = vec![Opcode::Var(VarId(0)), Opcode::Var(VarId(1)), Opcode::EQ];
        let mut storage = vec![
            [1..3, 5..15, 20..23].iter().cloned().collect(),
            [14..22].iter().cloned().collect(),
        ];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![14, 20, 21])
    }

    #[test]
    fn neq_const() {
        let code = vec![Opcode::Var(VarId(0)), Opcode::Const(22), Opcode::NEQ];
        let mut storage = vec![[1..3, 20..23].iter().cloned().collect()];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(
            storage[0].iter().collect::<Vec<isize>>(),
            vec![1, 2, 20, 21]
        )
    }

    #[test]
    fn eq_two_vars_add() {
        let code = vec![
            Opcode::Var(VarId(0)),
            Opcode::Var(VarId(1)),
            Opcode::Add,
            Opcode::Const(20),
            Opcode::EQ,
        ];
        let mut storage = vec![
            [1..3, 5..15, 20..23].iter().cloned().collect(),
            [14..22].iter().cloned().collect(),
        ];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![1, 2, 5, 6]);
        assert_eq!(
            storage[1].iter().collect::<Vec<isize>>(),
            vec![14, 15, 18, 19]
        );
    }

    #[test]
    fn abs_infer() {
        let code = vec![
            Opcode::Var(VarId(0)),
            Opcode::Abs,
            Opcode::Var(VarId(1)),
            Opcode::EQ,
        ];
        let mut storage = vec![
            [1..3, -15..5, 20..23].iter().cloned().collect(),
            [13..21].iter().cloned().collect(),
        ];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(
            storage[0].iter().collect::<Vec<isize>>(),
            vec![-15, -14, -13, 20]
        );
    }

    #[test]
    fn mul_infer() {
        let code = vec![
            Opcode::Var(VarId(0)),
            Opcode::Var(VarId(1)),
            Opcode::Mul,
            Opcode::Const(40),
            Opcode::EQ,
        ];
        let mut storage = vec![
            [1..6, 20..23].iter().cloned().collect(),
            [2..3, 8..9, 100..120].iter().cloned().collect(),
        ];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![5, 20]);
        assert_eq!(storage[1].iter().collect::<Vec<isize>>(), vec![2, 8]);
    }

    #[test]
    fn div_infer() {
        let code = vec![
            Opcode::Var(VarId(0)),
            Opcode::Var(VarId(1)),
            Opcode::Div,
            Opcode::Const(10),
            Opcode::EQ,
        ];
        let mut storage = vec![
            [10..15, 80..92].iter().cloned().collect(),
            [1..2, 7..14].iter().cloned().collect(),
        ];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![10, 80, 90]);
        assert_eq!(storage[1].iter().collect::<Vec<isize>>(), vec![1, 8, 9]);
    }

    #[test]
    fn mod_infer() {
        let code = vec![
            Opcode::Var(VarId(0)),
            Opcode::Const(10),
            Opcode::Mod,
            Opcode::Const(1),
            Opcode::EQ,
        ];
        let mut storage = vec![[10..15, 88..92].iter().cloned().collect()];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![11, 91]);
    }

    #[test]
    fn sub_infer() {
        let code = vec![
            Opcode::Var(VarId(0)),
            Opcode::Var(VarId(1)),
            Opcode::Sub,
            Opcode::Const(20),
            Opcode::EQ,
        ];
        let mut storage = vec![
            [30..35].iter().cloned().collect(),
            [5..12].iter().cloned().collect(),
        ];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![30, 31]);
        assert_eq!(storage[1].iter().collect::<Vec<isize>>(), vec![10, 11]);
    }

    #[test]
    fn sub_infer_negative() {
        let code = vec![
            Opcode::Var(VarId(0)),
            Opcode::Var(VarId(1)),
            Opcode::Sub,
            Opcode::Const(-10),
            Opcode::EQ,
        ];
        let mut storage = vec![
            [10..11].iter().cloned().collect(),
            [20..23].iter().cloned().collect(),
        ];

        infer_domains_inplace(&code, &mut storage).unwrap();

        assert_eq!(storage[0].iter().collect::<Vec<isize>>(), vec![10]);
        assert_eq!(storage[1].iter().collect::<Vec<isize>>(), vec![20]);
    }
}

use std::{cmp, iter::Step, ops::Range};

pub fn order<T: Ord>(left: Range<T>, right: Range<T>) -> (Range<T>, Range<T>) {
    if &left.start < &right.start {
        (left, right)
    } else {
        (right, left)
    }
}

pub fn is_empty<T: Ord>(r: &Range<T>) -> bool {
    r.start >= r.end
}

pub fn intersection<T: Ord>(left: Range<T>, right: Range<T>) -> Range<T> {
    let (left, right) = order(left, right);

    left.end..right.start
}

pub fn overlaps<T: Ord>(left: Range<T>, right: Range<T>) -> bool {
    !is_empty(&intersection(left, right))
}

pub fn merge<T: Ord>(left: Range<T>, right: Range<T>) -> Range<T> {
    cmp::min(left.start, right.start)..cmp::max(left.end, right.end)
}

pub fn difference<T: Ord + Step>(
    left: Range<T>,
    right: Range<T>,
) -> (Option<Range<T>>, Option<Range<T>>) {
    if &left.start >= &right.end || &left.end <= &right.start {
        // no overlap
        (Some(left), None)
    } else if &left.end <= &right.end && &left.start >= &right.start {
        // full deletion
        (None, None)
    } else if &left.end >= &right.end && &left.start <= &right.start {
        // split
        (Some(left.start..right.start), Some(right.end..left.end))
    } else if &left.start < &right.start && &left.end < &right.end {
        // right part deleted
        (Some(left.start..right.start), None)
    } else {
        // must be left part deleted
        (Some(right.end..left.end), None)
    }
}

pub fn abs(r: Range<isize>) -> Range<isize> {
    if r.start > 0 {
        r
    } else if r.start < 0 && r.end > 0 {
        0..cmp::max(r.start.abs() + 1, r.end)
    } else {
        r.end.abs() + 1..r.start.abs() + 1
    }
}

/// (0..10) -> (-9..1)
/// (5..10) -> (-9..-4)
pub fn negate(r: Range<isize>) -> Range<isize> {
    -r.end + 1..-r.start + 1
}

pub fn fix(r: Range<isize>) -> Range<isize> {
    if r.start < r.end - 1 {
        r
    } else {
        r.end - 1..r.start + 1
    }
}

pub fn add(x: Range<isize>, y: Range<isize>) -> Range<isize> {
    x.start.saturating_add(y.start)..x.end.saturating_add(y.end) - 1
}

pub fn sub(x: Range<isize>, y: Range<isize>) -> Range<isize> {
    add(x, negate(y))
}

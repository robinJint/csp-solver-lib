use std::{
    cmp, fmt,
    iter::{ExactSizeIterator, Extend, FromIterator, Peekable, Step},
    ops::{Deref, Range},
    rc::Rc,
};

use smallvec::{smallvec, SmallVec};

use crate::iter;
use crate::range_util;

#[derive(Clone, PartialEq, Default)]
pub struct RangeVec<T> {
    data: std::rc::Rc<SmallVec<[Range<T>; 2]>>,
}

impl<T: fmt::Debug> fmt::Debug for RangeVec<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.data.fmt(f)
    }
}

impl<T> Deref for RangeVec<T> {
    type Target = [Range<T>];
    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl<T: Ord + Copy + Step + fmt::Debug> RangeVec<T> {
    pub fn single(range: Range<T>) -> Self {
        Self {
            data: smallvec![range].into(),
        }
    }

    pub fn is_unit(&self) -> bool {
        self.data.len() == 1 && Step::forward(self.data[0].start, 1) == self.data[0].end
    }

    pub fn from_vec(data: impl Into<SmallVec<[Range<T>; 2]>>) -> Self {
        let mut data = data.into();
        data.sort_unstable_by_key(|r| r.start);

        Self::from_sorted_vec(data.into())
    }

    pub fn from_sorted_vec(mut data: SmallVec<[Range<T>; 2]>) -> Self {
        Self::optimize(&mut data);
        Self { data: data.into() }
    }

    // pub unsafe fn from_rangevec_iter(it: impl Iterator<Item = Range<T>>) -> Self {
    //     let mut data: SmallVec<Range<T>> = SmallVec::with_capacity(it.size_hint().0);
    //     it.for_each(|range| {
    //         let end = data.as_mut_ptr().add(data.len());
    //         std::ptr::write(end, range);
    //         data.set_len(data.len() + 1);
    //     });

    //     Self { data: data.into() }
    // }

    fn optimize(data: &mut SmallVec<[Range<T>; 2]>) {
        // println!("len: {}", self.data.len());
        if data.len() < 2 {
            return;
        }

        let mut current = 0;
        let mut next = 1;
        while next < data.len() {
            if data[current].end > data[next].end {
                next += 1;
            } else if data[current].end >= data[next].start {
                data[current].end = data[next].end;
                next += 1;
            } else {
                current += 1;
                data[current] = data[next].clone();
                next += 1;
            }
        }

        data.truncate(current + 1)
    }

    fn optimize_iter(it: impl Iterator<Item = Range<T>>) -> impl Iterator<Item = Range<T>> {
        struct OptimizeIter<It: Iterator> {
            inner: It,
            current: Option<It::Item>,
        }

        impl<It: Iterator<Item = Range<T>>, T: Ord + Copy> Iterator for OptimizeIter<It> {
            type Item = Range<T>;

            fn next(&mut self) -> Option<Self::Item> {
                let current = match self.current.take() {
                    Some(x) => x,
                    None => self.inner.next()?,
                };

                let next = match self.inner.next() {
                    Some(x) => x,
                    None => return Some(current),
                };

                if current.end > next.end {
                    None // wrong?
                } else if current.end >= next.start {
                    self.current = Some(current.start..next.end);
                    self.next()
                } else {
                    self.current = Some(next);
                    Some(current)
                }
            }

            fn fold<B, F>(mut self, init: B, mut f: F) -> B
            where
                F: FnMut(B, Self::Item) -> B,
            {
                let first = if let Some(first) = self.current {
                    first
                } else {
                    if let Some(first) = self.inner.next() {
                        first
                    } else {
                        return init;
                    }
                };

                let (f_accum, accum) =
                    self.inner.fold((init, first), |(f_accum, current), next| {
                        if current.end > next.end {
                            (f_accum, current)
                        } else if current.end >= next.start {
                            (f_accum, current.start..next.end)
                        } else {
                            let f_accum = f(f_accum, current);
                            (f_accum, next)
                        }
                    });

                f(f_accum, accum)
            }

            fn size_hint(&self) -> (usize, Option<usize>) {
                let (min, max) = self.inner.size_hint();
                let has_current = self.current.is_some() as usize;
                (min + has_current, max.map(|max| max + has_current))
            }
        }

        OptimizeIter {
            inner: it,
            current: None,
        }
    }

    pub fn iter_ranges(&self) -> RangeVecIter<T> {
        RangeVecIter {
            inner: self.data.iter(),
        }
    }

    pub fn iter(&self) -> RangeVecIterValues<T> {
        RangeVecIterValues {
            inner: self.iter_ranges().cloned().flatten(),
        }
    }

    pub fn merge(&self, other: &Self) -> Self {
        let mut new_vec = SmallVec::with_capacity(self.data.len() + other.data.len());

        let mut it1 = self.iter_ranges().peekable();
        let mut it2 = other.iter_ranges().peekable();

        while let (Some(l), Some(r)) = (it1.peek(), it2.peek()) {
            if l.start < r.start {
                new_vec.push((*l).clone());
                it1.next().unwrap();
            } else {
                new_vec.push((*r).clone());
                it2.next().unwrap();
            }
        }

        new_vec.extend(it1.cloned());
        new_vec.extend(it2.cloned());

        Self::from_sorted_vec(new_vec)
    }

    pub fn merge_inline(
        it1: impl Iterator<Item = Range<T>>,
        it2: impl Iterator<Item = Range<T>>,
    ) -> impl Iterator<Item = Range<T>> {
        Self::optimize_iter(iter::merge_iter_by(it1, it2, |r1, r2| {
            r1.start.cmp(&r2.start)
        }))
    }

    pub fn intersection(&self, other: &Self) -> Self {
        let mut new_vec = SmallVec::with_capacity(cmp::max(self.data.len(), other.data.len()));

        let mut l_it = self.iter_ranges().peekable();
        let mut r_it = other.iter_ranges().peekable();

        while let (Some(l), Some(r)) = (l_it.peek(), r_it.peek()) {
            let (l, r, l_it, r_it) = if l.start < r.start {
                (*l, *r, &mut l_it, &mut r_it)
            } else {
                (*r, *l, &mut r_it, &mut l_it)
            };

            if l.end > r.start {
                if l.end >= r.end {
                    r_it.next().unwrap();
                    new_vec.push(r.start..r.end);
                } else {
                    l_it.next().unwrap();
                    new_vec.push(r.start..l.end);
                }
            } else {
                l_it.next().unwrap();
            }
        }

        Self {
            data: new_vec.into(),
        }
    }

    pub fn remove(&mut self, val: T) {
        let data = Rc::make_mut(&mut self.data);
        for i in 0..data.len() {
            let current = &data[i];
            if current.contains(&val) {
                let l = current.start..val;
                let r = Step::forward(val, 1)..current.end;

                if range_util::is_empty(&l) {
                    data[i] = r;
                } else if range_util::is_empty(&r) {
                    data[i] = l;
                } else {
                    data[i] = l;
                    data.insert(i + 1, r)
                }
            }
        }
    }

    pub fn product_with(
        &self,
        other: &Self,
        mut f: impl FnMut(Range<T>, Range<T>) -> Range<T>,
    ) -> Self {
        let mut new_vec = Vec::with_capacity(self.data.len() * other.data.len());

        for r1 in self.iter_ranges() {
            for r2 in other.iter_ranges() {
                new_vec.push(f(r1.clone(), r2.clone()))
            }
        }

        Self::from_vec(new_vec)
    }
}

impl RangeVec<isize> {
    pub fn len(&self) -> usize {
        self.data.iter().map(|range| range.len()).sum()
    }
}

impl<A: Ord + Copy + Step + fmt::Debug> FromIterator<Range<A>> for RangeVec<A> {
    fn from_iter<T: IntoIterator<Item = Range<A>>>(iter: T) -> Self {
        let data: SmallVec<_> = iter.into_iter().collect();
        Self::from_vec(data)
    }
}

impl<T: Ord + Step + fmt::Debug + Copy> From<Range<T>> for RangeVec<T> {
    fn from(range: Range<T>) -> Self {
        Self::single(range)
    }
}

pub struct RangeVecIter<'a, T> {
    inner: std::slice::Iter<'a, Range<T>>,
}

impl<'a, T> Iterator for RangeVecIter<'a, T> {
    type Item = &'a Range<T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

pub struct RangeVecIterValues<'a, T: Step> {
    inner: std::iter::Flatten<std::iter::Cloned<RangeVecIter<'a, T>>>,
}

impl<'a, T: Step> Iterator for RangeVecIterValues<'a, T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn optimize() {
        let mut input: SmallVec<[_; 2]> = smallvec![0..5, 3..10, 15..17, 20..30, 22..26];
        RangeVec::optimize(&mut input);
        assert_eq!(input.as_ref(), &[0..10, 15..17, 20..30]);
    }

    #[test]
    fn merge() {
        let set1: RangeVec<isize> = [5isize..7, 9..10].iter().cloned().collect();
        let set2: RangeVec<isize> = [10isize..13].iter().cloned().collect();

        let result = RangeVec::merge(&set1, &set2);

        assert_eq!(
            result.iter_ranges().cloned().collect::<Vec<_>>(),
            vec![5..7, 9..13]
        );
    }

    #[test]
    fn product() {
        let set1: RangeVec<isize> = [5isize..7, 9..10].iter().cloned().collect();
        let set2: RangeVec<isize> = [10isize..13].iter().cloned().collect();

        let result = RangeVec::product_with(&set1, &set2, |r1, r2| {
            (r1.start + r2.start)..(r1.end + r2.end - 1)
        });

        assert_eq!(
            result.iter_ranges().cloned().collect::<Vec<_>>(),
            vec![15..22]
        );
        assert_eq!(
            result.iter().collect::<Vec<_>>(),
            vec![15, 16, 17, 18, 19, 20, 21]
        );
    }

    #[test]
    fn intersection() {
        let set1: RangeVec<isize> = [5isize..7, 9..12, 14..16, 19..22].iter().cloned().collect();
        let set2: RangeVec<isize> = [10isize..20].iter().cloned().collect();

        let result = RangeVec::intersection(&set1, &set2);

        assert_eq!(
            result.iter_ranges().cloned().collect::<Vec<_>>(),
            vec![10..12, 14..16, 19..20]
        );
    }
}
